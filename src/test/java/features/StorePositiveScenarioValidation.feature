Feature: Validating Positive scenarios for Store

@RegressionTest
Scenario Outline: Verify if user is able to place order for a pet
Given User gives the order details
		|OrderID		|<OrderID>		|
		|PetID			|<PetID>		|
		|Quantity		|<Quantity>		|
		|ShipDate		|<ShipDate>		|
		|Status			|<Status>		|
		|Complete		|<Complete>		|
		
When user call "PostStoreOrder" with "POST" http request
Then the API call should get success with status code 200

Examples:
|OrderID	|PetID		|Quantity	|ShipDate					|Status			|Complete		|
|100		|1234		|2			|2020-09-26T21:45:33.542Z	|Placed			|true			|


@RegressionTest
Scenario Outline: Verify if user is able to find the Order based on Order ID
Given User gives "OrderID"
When user call "GetStoreOrderID" with "GET" http request for <OrderID>
Then the API call should get success with status code 200
And "id" in the response body should be "<OrderID>"

Examples:
|OrderID	|
|5			|
|7			|


@RegressionTest
Scenario Outline: Verify if user user is able to delete the Order on based on Order ID
Given User gives "OrderID"
When user call "GetStoreOrderID" with "Delete" http request for <OrderID>
Then the API call should get success with status code 200

Examples:
|OrderID	|
|100		|


@RegressionTest @Test
Scenario: Verify if user is able to get all details of Store inventory
Given user sends api_key "special-key"
When user call "GetStoreInventory" with "Get" http request
Then the API call should get success with status code 200
