Feature: Validating Negative scenarios for User

@RegressionTest
Scenario Outline: Verify if user is not able to find the User based on wrong username
Given User gives "username"
When user call "GetUser" with "GET" http request for <username>
Then the API call should get success with status code 404

Examples:
|username		|
|abkbfsfebf		|


@RegressionTest
Scenario Outline: Verify if user is not able to find the User based on invalid username
Given User gives "username"
When user call "GetUser" with "GET" http request for <username>
Then the API call should get success with status code 400

Examples:
|username		|
|as32$#13		|


@RegressionTest
Scenario Outline: Verify if user is not able to update wrong username
Given User gives the updated user details
	|userId			|5345			|
	|userName		|test1			|
	|firstName		|test			|
	|lastName		|automation		|
	|email			|ttaut@gmail.com|
	|password		|testing		|
	|phone			|4730297		|
	|userStatus		|0				|
	
When user call "GetUser" with "PUT" http request for <username>
Then the API call should get success with status code 404

Examples:
|username		|
|hfkehwoh		|


@RegressionTest
Scenario Outline: Verify if user is not able to update invalid username
Given User gives the updated user details
	|userId			|5345			|
	|userName		|test1			|
	|firstName		|test			|
	|lastName		|automation		|
	|email			|ttaut@gmail.com|
	|password		|testing		|
	|phone			|4730297		|
	|userStatus		|0				|
	
When user call "GetUser" with "PUT" http request for <username>
Then the API call should get success with status code 400

Examples:
|username		|
|ad$#%123456	|


@RegressionTest
Scenario Outline: Verify is user is not able to delete the wrong username account
Given User gives "username"
When user call "GetUser" with "Delete" http request for <username>
Then Validate if login is successful then only status code should be 404 


Examples:
|username	|password		|
|dhlhsdl	|rstuvw			|


@RegressionTest
Scenario Outline: Verify is user is not able to delete the invalid username account
Given User gives "username"
When user call "GetUser" with "Delete" http request for <username>
Then Validate if login is successful then only status code should be 400 

Examples:
|username	|
|100		|


@RegressionTest
Scenario Outline: Verify if user is not able to login with invalid username/password
Given User sends the username <username> and password <password>
When user call "GetUserLogin" with "Get" http request
Then the API call should get success with status code 400

Examples:
|username	|password	|
|$#%#$		|423242		|

