Feature: Validating Negative scenarios for Store

@RegressionTest
Scenario: Verify if user is not able to place order for a pet with invalid input
Given User gives invalid order details
When user call "PostStoreOrder" with "POST" http request
Then the API call should get success with status code 400

@RegressionTest
Scenario: Verify if user is not able to find the Order based on wrong Order ID
Given User gives "OrderID"
When user call "GetStoreOrderID" with "GET" http request for 10054335
Then the API call should get success with status code 404

@RegressionTest
Scenario Outline: Verify if user is not able to find the Order based on invalid Order ID
Given User gives "OrderID"
When user call "GetStoreOrderID" with "GET" http request for <OrderID>
Then the API call should get success with status code 400

Examples:
|OrderID	|
|fsfd54		|

@RegressionTest
Scenario: Verify if user is not able to delete the Order based on wrong Order ID
Given User gives "OrderID"
When user call "GetStoreOrderID" with "Delete" http request for 470237027
Then the API call should get success with status code 404

@RegressionTest
Scenario Outline: Verify if user is not able to delete the Order based on invalid Order ID
Given User gives "OrderID"
When user call "GetStoreOrderID" with "Delete" http request for <OrderID>
Then the API call should get success with status code 400

Examples:
|OrderID	|
|dbk$#@d	|