Feature: Validating Positive scenarios for Pet

@RegressionTest
Scenario: Verify image is getting uploaded using UploadImageAPI
Given Upload Image with "22" "Additional_Data" and "PetImage.jpg"
When user call "UploadImageAPI" with "Post" http request
Then the API call should get success with status code 200

@RegressionTest
Scenario Outline: Verify user is able to add Pet
Given User sends the data for Pet details
	  | petID        | <petid>        	|
      | categoryID   | <categoryid>   	|
      | categoryName | <categoryname> 	|
      | petName      | <petname>     	|
      | photoUrls    | <petphotoUrls>   |
      | tagsID       | <tagid>     		|
      | tagsName     | <tagname>     	|
      | petStatus    | <petstatus>   	|
When user call "PetAPI" with "<request>" http request
Then the API call should get success with status code 200

Examples:
|petid		|categoryid	|categoryname	|petname|petphotoUrls	|tagid	  |tagname	   |petstatus|request	|
|2222 		|122		|German	  		|Bruno  |PetImage1		|222	  |Tag1		   |Available|Post	 	|


@RegressionTest
Scenario Outline: Verify if user is able to update pet
Given User sends the data for Pet details
	  | petID        | <petid>        	|
      | categoryID   | <categoryid>   	|
      | categoryName | <categoryname> 	|
      | petName      | <petname>     	|
      | photoUrls    | <petphotoUrls>   |
      | tagsID       | <tagid>     		|
      | tagsName     | <tagname>     	|
      | petStatus    | <petstatus>   	|
When user call "PetAPI" with "Put" http request
Then the API call should get success with status code 200

Examples:
|petid		|categoryid	|categoryname	|petname	|petphotoUrls	|tagid	  |tagname	   |petstatus|
|1234 		|122		|Labra	  		|Koko	  	|PetImage1		|222	  |Tag1		   |Available|
|6666 		|122		|Labra	  		|Koko	  	|PetImage1		|222	  |Tag1		   |Available|


@RegressionTest
Scenario Outline: Verify user is able to get response having Pet Details for selected Pet Status
Given User sends the status <status>
When user call "GetPetStatus" with "GET" http request
Then the API call should get success with status code 200
And Response pet list should have status "<status>"

Examples:
|status		|
|available	|
|Sold		|


@RegressionTest
Scenario Outline: Verify if user is able to get Pet ID
Given User gives "PetID"
When user call "GetPetID" with "GET" http request for <petID>
Then the API call should get success with status code 200

Examples:
|petID				|
|1234				|


@RegressionTest
Scenario Outline: Verify if user is able to update an existing PetID
Given User sends PetID with "<name>" "<status>"
When user call "GetPetID" with "Post" http request for <petID>
Then the API call should get success with status code 200

Examples:
|petID		|name		|status		|
|1234		|KoKo		|Pending	|


@RegressionTest
Scenario Outline: Verify if user is able to delete an existing PetID
Given user sends api_key "special-key"
When user call "GetPetID" with "DELETE" http request for <petID>
Then the API call should get success with status code 200

Examples:
|petID		|
|2222 		|
