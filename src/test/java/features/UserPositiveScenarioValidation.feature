Feature: Validating Postive scenarios for User

@RegressionTest
Scenario Outline: Verify if multiple users can be created
Given User gives user details in the form of array
	|userId			|<userID>		|
	|userName		|<userName>		|
	|firstName		|<firstName>	|
	|lastName		|<lastName>		|
	|email			|<email>		|
	|password		|<password>		|
	|phone			|<phone>		|
	|userStatus		|<userStatus>	|

When user call "PostUserArray" with "POST" http request
Then the API call should get success with status code 200

Examples:
|userID		|userName		|firstName		|lastName		|email				|password		|phone			|userStatus		|
|0			|abcdef			|abc			|def			|abcdef@gmail.com	|abcdef			|681234567		|0				|


@RegressionTest
Scenario: Verify if multiple users can be created
Given User gives list of array of user details
	
|userId		|userName		|firstName		|lastName		|email				|password		|phone			|userStatus		|
|0			|testaut		|test			|aut			|testaut@gmail.com	|testaut		|681234567		|0				|	
|0			|ghijkl			|ghi			|jkl			|abcdef@gmail.com	|ghijkl			|79789879		|0				|
	
When user call "PostUserArray" with "POST" http request
Then the API call should get success with status code 200


@RegressionTest
Scenario Outline: Verify if user is able to find the User based on username
Given User gives "username"
When user call "GetUser" with "GET" http request for <username>
Then the API call should get success with status code 200


Examples:
|username	|
|abcdef		|


@RegressionTest
Scenario Outline: Verify if user is able to update the username
Given User gives the updated user details
	|userId			|5345			|
	|userName		|abcdef			|
	|firstName		|test			|
	|lastName		|automation		|
	|email			|ttaut@gmail.com|
	|password		|testing		|
	|phone			|4730297		|
	|userStatus		|0				|
	
When user call "GetUser" with "PUT" http request for <username>
Then the API call should get success with status code 200

Examples:
|username		|
|abcdef			|


@RegressionTest
Scenario Outline: Verify is user is able to delete the account
Given User is logged in with the username <username> and password <password>
And User gives "username"
When user call "GetUser" with "Delete" http request for <username>
Then Validate if login is successful then only status code should be 200 

Examples:
|username	|password		|
|ghijkl		|ghijkl			|


@RegressionTest
Scenario Outline: Verify if user is able to login
Given User sends the username <username> and password <password>
When user call "GetUserLogin" with "Get" http request
Then the API call should get success with status code 200

Examples:
|username	|password	|
|abcdef		|abcdef		|


@RegressionTest
Scenario Outline: Verify if user is able to logout from current session
Given User is logged in with the username <username> and password <password>
And No Additional Details given
When user call "GetUserLogout" with "Get" http request
Then the API call should get success with status code 200

Examples:
|username	|password		|
|abcdef		|abcdef			|


@RegressionTest @Test
Scenario Outline: Verify if user is able to create a User
Given User gives user details to create User
	|userId			|<userID>		|
	|userName		|<userName>		|
	|firstName		|<firstName>	|
	|lastName		|<lastName>		|
	|email			|<email>		|
	|password		|<password>		|
	|phone			|<phone>		|
	|userStatus		|<userStatus>	|

When user call "PostUser" with "POST" http request
Then the API call should get success with status code 200

Examples:
|userID		|userName		|firstName		|lastName		|email				|password		|phone			|userStatus		|
|0			|abcdef			|abc			|def			|abcdef@gmail.com	|abcdef			|681234567		|0				|
|0			|ghujkl			|ghi			|jkl			|ghijkl@gmail.com	|ghijkl			|890123456		|0				|

