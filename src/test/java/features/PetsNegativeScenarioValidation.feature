Feature: Validating Negative scenarios for Pet

@RegressionTest
Scenario Outline: Verify user is not able to add Pet with wrong input
Given User sends the data for Pet details
	  | petID        | <petid>        	|
      | categoryID   | <categoryid>   	|
      | categoryName | <categoryname> 	|
      | petName      | <petname>     	|
      | photoUrls    | <petphotoUrls>   |
      | tagsID       | <tagid>     		|
      | tagsName     | <tagname>     	|
      | petStatus    | <petstatus>   	|
When user call "PetAPI" with "<request>" http request
Then the API call should get success with status code 405

Examples:
|petid		|categoryid	|categoryname	|petname|petphotoUrls	|tagid	  |tagname	   |petstatus|request	|
|1234 		|122		|German	  		|Bruno  |PetImage1		|222	  |Tag1		   |Available|Delete 	|


@RegressionTest
Scenario Outline: Verify if user is not able to update wrong pet ID
Given User sends the data for Pet details
	  | petID        | <petid>        	|
      | categoryID   | <categoryid>   	|
      | categoryName | <categoryname> 	|
      | petName      | <petname>     	|
      | photoUrls    | <petphotoUrls>   |
      | tagsID       | <tagid>     		|
      | tagsName     | <tagname>     	|
      | petStatus    | <petstatus>   	|
When user call "PetAPI" with "Put" http request
Then the API call should get success with status code 404

Examples:
|petid		|categoryid	|categoryname	|petname	|petphotoUrls	|tagid	  |tagname	   |petstatus	|
|3682736	|122		|Labra	  		|Koko	  	|PetImage1		|222	  |Tag1		   |Available	|


@RegressionTest
Scenario Outline: Verify if user is not able to update pet with invalid Pet Id
Given User sends invalid Pet ID "<PetID>"
When user call "PetAPI" with "Put" http request
Then the API call should get success with status code 400

Examples:
|PetID		|
|sd43$#ff	|


@RegressionTest
Scenario Outline: Verify if user is not able to update pet with wrong request
Given User sends the data for Pet details
	  | petID        | <petid>        	|
      | categoryID   | <categoryid>   	|
      | categoryName | <categoryname> 	|
      | petName      | <petname>     	|
      | photoUrls    | <petphotoUrls>   |
      | tagsID       | <tagid>     		|
      | tagsName     | <tagname>     	|
      | petStatus    | <petstatus>   	|
When user call "PetAPI" with "Delete" http request
Then the API call should get success with status code 405

Examples:
|petid		|categoryid	|categoryname	|petname	|petphotoUrls	|tagid	  |tagname	   |petstatus	|
|1234		|122		|Labra	  		|Koko	  	|PetImage1		|222	  |Tag1		   |test		|


@RegressionTest
Scenario Outline: Verify user is not able to get response having Pet Details for invalid Pet Status
Given User sends the status <status>
When user call "GetPetStatus" with "GET" http request
Then the API call should get success with status code 400

Examples:
|status		|
|343534		|


@RegressionTest
Scenario: Verify if user is not able to get wrong Pet ID
Given User gives "PetID"
When user call "GetPetID" with "GET" http request for 123432
Then the API call should get success with status code 404


@RegressionTest
Scenario Outline: Verify if user is not able to get invalid Pet ID
Given User gives "PetID"
When user call "GetPetID" with "GET" http request for <PetID>
Then the API call should get success with status code 400

Examples:
|PetID		|
|abcdef		|


@RegressionTest
Scenario Outline: Verify if user is not able to update Pet with an invalid request
Given User sends PetID with "<name>" "<status>"
When user call "GetPetID" with "Put" http request for <petID>
Then the API call should get success with status code 405

Examples:
|petID		|name		|status		|
|5555		|KoKo		|Pending	|


@RegressionTest
Scenario: Verify if user is not able to delete a wrong PetID
Given user sends api_key "special-key"
When user call "GetPetID" with "Delete" http request for 3333
Then the API call should get success with status code 404


@RegressionTest
Scenario Outline: Verify if user is not able to delete an invalid PetID
Given user sends api_key "special-key"
When user call "GetPetID" with "DELETE" http request for <PetID>
Then the API call should get success with status code 400

Examples:
|PetID		|
|abcd		|
