package stepDefinations;

import Resources.Utils;
import cucumber.api.java.en.*;
import io.restassured.http.ContentType;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;
import java.util.Map;


public class StepDefinationsUser extends Utils{
	
	
	int LoginResponse;
	
	@Given("User gives user details in the form of array")
	public void User_gives_user_details_in_the_form_of_array(Map<String,String> user) throws IOException {
	    
		res = given().spec(requestSpecification()).contentType(ContentType.JSON).
				body("[" + data.CreateUserWithArray(user) + "]");
		
	}
	
	@Given("User gives list of array of user details")
	public void user_gives_list_of_array_of_user_details(List<Map<String,String>> users) throws IOException {
	    
		String body = null;
		for(int i=0; i< users.size();i++) {
			
			if(i == 0)
				body = data.CreateUserWithList(users, i).toJSONString();
			else
				body = body + "," + data.CreateUserWithList(users, i).toJSONString();
			
		}
		
		res = given().spec(requestSpecification()).contentType(ContentType.JSON).
				body("[" + body + "]");
		
	}
	
	@Given("User gives the updated user details")
	public void user_gives_updated_user_details(Map<String,String> user) throws IOException {
	    
		res = given().spec(requestSpecification()).contentType(ContentType.JSON)
				.body(data.CreateUserWithArray(user));
		
	}
	
	@Given("User sends the username {} and password {}")
	public void user_sends_the_username_and_password(String username, String password) throws IOException {
	    
		res = given().spec(requestSpecification()).queryParam("username", username).
				queryParam("password", password);
		
	}
	
	@Given("User is logged in with the username {} and password {}")
	public void user_is_logged_in_with_the_username_and_password(String username, String password) throws IOException {
	    
		user_sends_the_username_and_password(username, password);
		BaseStepDefinations bsd = new BaseStepDefinations();
		bsd.user_call_with_http_request("GetUserLogin", "Get");
		
		LoginResponse = response.getStatusCode();
		
	}
	
	@Given("No Additional Details given")
	public void No_Additional_Details_given() throws IOException {
		
		res = given().spec(requestSpecification());
		
	}
	
	@Then("Validate if login is successful then only status code should be {int}")
	public void validate_if_login_is_successful_then_only_status_code_should_be(int statuscode) {
	    
		int DeleteResponse = response.getStatusCode();
		
		if (LoginResponse == 200)
			assertEquals(statuscode, DeleteResponse);
		else if (DeleteResponse == 200)
			assertFalse("Delete API should have failed for failed login", false);
		else
			assertTrue("Delete API can only be called on successful login", true);
	}
	
	@Given("User gives user details to create User")
	public void user_gives_user_details_to_create_user(Map<String,String> user) throws IOException {
	    
		res = given().spec(requestSpecification()).contentType(ContentType.JSON).
				body(data.CreateUserWithArray(user));
		
	}
	
}
