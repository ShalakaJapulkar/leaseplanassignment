package stepDefinations;

import cucumber.api.java.en.*;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import Resources.Utils;

public class StepDefinationsPets extends Utils{

	static String ID;
	
	@Given("Upload Image with {string} {string} and {string}")
	public void upload_image_with_and(String PetID, String additionalData, String ImageName) throws IOException {

		File imageFile = new File(System.getProperty("user.dir")+"//src//test//java//Resources//" + ImageName);
		
		resspec = new ResponseSpecBuilder().expectStatusCode(200).expectContentType(ContentType.JSON).build();
		
		res = given().spec(requestSpecification()).pathParam("ID", PetID).
				formParam("additionalMetadata", additionalData).contentType("multipart/form-data").
				multiPart("file", imageFile, "image/jpeg");
		
	}
	
	@Given("User sends the data for Pet details")
	public void User_sends_the_data_for_Pet_details(Map<String, String> pets) throws IOException {
		res = given().spec(requestSpecification()).contentType(ContentType.JSON).
				body(data.AddAndUpdatePet(pets));
	}
	
	@Given("User sends the status {}")
	public void user_sends_the_status(String status) throws IOException {
	    
		res = given().spec(requestSpecification()).contentType(ContentType.JSON).
				queryParam("status", status);
		
	}
	
	@Given("User sends PetID with {string} {string}")
	public void user_sends_pet_id_with(String name, String status) throws IOException {
	    res=given().spec(requestSpecification()).contentType("application/x-www-form-urlencoded").
	    		formParam("name", name).
	    		formParam("status", status);
	}
	
	@Given("user sends api_key {string}")
	public void user_sends_api_key(String apikey) throws IOException {
	    	    
		res = given().spec(requestSpecification())
	    		.queryParam("api_key", apikey);
	}
	
	@Given("User sends invalid Pet ID {string}")
	public void user_sends_invalid_Pet_details(String PetID) throws IOException {
	    
		res = given().spec(requestSpecification()).contentType(ContentType.JSON).
				body(data.UpdateInvalidPet(PetID));
		
	}
	
	@Then("{string} in the response body should be {string}")
	public void in_the_response_body_should_be(String key, String value) {
		
		assertEquals(getJsonPath(response, key), value);	
		
	}
	
	@Then("Response pet list should have status {string}")
	public void Response_pet_list_should_have_status(String status) {
	   
		List<String> Status = response.jsonPath().getList("$.status");
		
		for (String status1: Status) {
			assertEquals(status, status1);
		}
	   
	}
	
}
