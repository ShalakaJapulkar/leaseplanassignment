package stepDefinations;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import Resources.APIResources;
import Resources.Utils;
import cucumber.api.java.en.*;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;

public class BaseStepDefinations extends Utils{
	
	@Given("User gives {string}")
	public void user_gives(String string) throws IOException {
	    
		StepDefinationsUser sdu = new StepDefinationsUser();
		sdu.No_Additional_Details_given();
		
	}
	
	@When("user call {string} with {string} http request")
	public void user_call_with_http_request(String resource, String method) {
	   		
		resspec = new ResponseSpecBuilder().expectContentType(ContentType.JSON).build();
		
		response = null;
		
		APIResources resourceAPI = APIResources.valueOf(resource);
		
		if (method.equalsIgnoreCase("POST"))
			response =res.when().post(resourceAPI.getResource());
		else if (method.equalsIgnoreCase("GET"))		
			response =res.when().get(resourceAPI.getResource());
		else if (method.equalsIgnoreCase("DELETE"))
			response = res.when().delete(resourceAPI.getResource());
		else if (method.equalsIgnoreCase("PUT"))		
			response =res.when().put(resourceAPI.getResource());
		
	}
	
	@When("user call {string} with {string} http request for {}")
	public void user_call_with_http_request_for(String resource, String method, String ID) {
	    
		APIResources resourceAPI = APIResources.valueOf(resource);
		
		if (method.equalsIgnoreCase("GET"))		
			response =res.when().get(resourceAPI.getResource().replace("{}", ID.toString()));
		else if (method.equalsIgnoreCase("DELETE"))
			response = res.when().delete(resourceAPI.getResource().replace("{}", ID.toString()));
		else if (method.equalsIgnoreCase("PUT"))
			response = res.when().put(resourceAPI.getResource().replace("{}", ID.toString()));
		
	}

	
	@Then("the API call should get success with status code {int}")
	public void the_api_call_should_get_success_with_status_code(int statuscode) {
	  
		assertEquals(statuscode, response.getStatusCode());

	}
	
}
