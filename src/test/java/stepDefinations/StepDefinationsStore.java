package stepDefinations;

import cucumber.api.java.en.*;
import io.restassured.http.ContentType;

import static io.restassured.RestAssured.given;

import java.io.IOException;
import java.util.Map;

import Resources.Utils;


public class StepDefinationsStore extends Utils{
	
	@Given("User gives the order details")
	public void user_gives_the_order_details(Map<String,String> store) throws IOException {
	    
		res = given().spec(requestSpecification()).contentType(ContentType.JSON).
				body(data.StoreOrder(store));
		
	}
	
	@Given("User gives invalid order details")
	public void user_gives_invalid_order_details() throws IOException {
	    
		res = given().spec(requestSpecification()).contentType(ContentType.JSON).
				body(data.PostInvalidOrder());
		
	}
	
	
}
