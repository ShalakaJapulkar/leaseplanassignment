package Resources;

//enum is special class in java which has collection of constants or methods

/**
  	This class helps to declare API resources in a constant 
  	which can be directly pass to the Scenarios in Feature file
 **/

public enum APIResources {

	UploadImageAPI("/pet/{ID}/uploadImage"),
	PetAPI("/pet"),
	GetPetStatus("/pet/findByStatus"),
	GetPetID("/pet/{}"),
	PostStoreOrder("store/order"),
	GetStoreOrderID("/store/order/{}"),
	GetStoreInventory("/store/inventory"),
	PostUserArray("/user/createWithArray"),
	GetUser("/user/{}"),
	GetUserLogin("/user/login"),
	GetUserLogout("/user/logout"),
	PostUser("/user");
	
	
	
	private String resource;
	
	APIResources(String resource)
	{
		this.resource = resource;
	}
	
	public String getResource() {
		return resource;
	}
	
}
