package Resources;

import org.json.simple.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * API calls where JSON body needs to be send in the requests are covered in this class.
 * All the below methods returns JSON Object for the particular rest calls
   which is then handled at step definition file for validation
 */


public class TestDataBuild {
	
	JSONObject body = new JSONObject();
	
	
	/**
     * This is method to send JSON body for adding and updating pet
     * @param pets
     * @return
     */
	
	public JSONObject AddAndUpdatePet(Map<String, String> pets) {
      
        body.clear();
        body.put("id", pets.get("petID"));
        
        Map<String, Object> category = new HashMap<>();
        category.put("id", pets.get("categoryID"));
        category.put("name", pets.get("categoryName"));
        body.put("category", category);
        body.put("name", pets.get("petName"));
        
        List<String> urlPhoto = new ArrayList<>();
        urlPhoto.add(pets.get("photoUrls"));
        
        body.put("photoUrls", urlPhoto);
        
        List<Map<String, Object>> tagsList = new ArrayList<>();

        Map<String, Object> tags = new HashMap<>();
        tags.put("id", pets.get("tagsID"));
        tags.put("name", pets.get("tagsName"));
        tagsList.add(tags);
        
        
        body.put("tags", tagsList);
        body.put("status", pets.get("petStatus"));
        
        return body;
		
	}
	
	
	/**
     * This is method to send JSON body for placing order
     * @param order
     * @return
     */
	public JSONObject StoreOrder(Map<String, String> order) {
		
		body.clear();
		body.put("id", order.get("OrderID"));
		body.put("petId", order.get("PetID"));
		body.put("quantity", order.get("Quantity"));
		body.put("shipDate", order.get("ShipDate"));
		body.put("status", order.get("Status"));
		body.put("complete", order.get("Complete"));
		return body;
	}
	
	
	/**
     * This is method to send JSON body for Creating User With Array
     * @param user
     * @return
     */
	public JSONObject CreateUserWithArray(Map<String,String> users) {
		
		body.clear();
		body.put("id", users.get("userId"));
		body.put("username", users.get("userName"));
		body.put("firstName", users.get("firstName"));
		body.put("lastName", users.get("lastName"));
		body.put("email", users.get("email"));
		body.put("password", users.get("password"));
		body.put("phone", users.get("phone"));
		body.put("userStatus", users.get("userStatus"));
		
		return body;
	}
	
	
	/**
     * This is method to send JSON body for Creating User With List
     * @param user
     * @return
     */
	public JSONObject CreateUserWithList(List<Map<String, String>> users, int i) {
		
		body.clear();
		
		
			Map<String, String> Details = new HashMap<>();
			Details.put("id", users.get(i).get("userId"));
			Details.put("username", users.get(i).get("userName"));
			Details.put("firstName", users.get(i).get("firstName"));
			Details.put("lastName", users.get(i).get("lastName"));
			Details.put("email", users.get(i).get("email"));
			Details.put("password", users.get(i).get("password"));
			Details.put("phone", users.get(i).get("phone"));
			Details.put("userStatus", users.get(i).get("userStatus"));
			
			body.putAll(Details);

		return body;
		
	}
	
	/**
     * This is method is used to send JSON body for Posting Invalid Order
     * @return
     */
	public String PostInvalidOrder () {
		
		return "{\r\n  \"id\": abcd,\r\n"+
		"\"petId\": 0,\r\n  \"quantity\": 0,\r\n"+
		"\"shipDate\": \"2020-09-28T11:26:00.210Z\",\r\n  "+
		"\"status\": \"placed\",\r\n  \"complete\": true\r\n}";
		
	}
	
	/**
     * This is method is used to send JSON body for Updating Invalid Pet
     * @return
     */
	public String UpdateInvalidPet(String PetID) {
		
		return "{\r\n  \"id\": " + PetID + ",\r\n  "+
		"\"category\": {\r\n    \"id\": 23,\r\n    "+
		"\"name\": \"test1\"\r\n  },\r\n  "+
		"\"name\": \"Koko\",\r\n  \"photoUrls\": [\r\n    "+
		"\"Image1\"\r\n  ],\r\n  \"tags\": [\r\n    "+
		"{\r\n      \"id\": 34,\r\n      \"name\": \"Tag1\"\r\n    "+
		"}\r\n  ],\r\n  \"status\": \"available\"\r\n}";
		
	}
	
}
